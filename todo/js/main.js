var app = angular.module('myApp', []);
app.controller('TodoCtrl', function($scope) {
    $scope.todos = [
       {text: 'learn angular', done: true},
       {text: 'build an angular app', done: false}];

    $scope.addTodo = function() {
        $scope.todos.push({text: $scope.todoText, done: false});
        $scope.todoText = '';
    };

    $scope.getTotalTodos = function() {
        return $scope.todos.length;
    };

    $scope.getRemainingTodos = function() {
        var count = 0;
        angular.forEach($scope.todos, function(todo) {
            count += todo.done ? 0 : 1;
        });
        return count;
    };

    $scope.archive = function() {
        var oldTodos = $scope.todos;
        $scope.todos = [];
        angular.forEach(oldTodos, function(todo) {
            if(!todo.done) $scope.todos.push(todo);
        });
    };

    $scope.clearCompleted = function() {
        $scope.todos = _.filter($scope.todos, function(todo){
            return !todo.done;
        });
    };
});