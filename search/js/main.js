var app = angular.module('myApp', []);
app.controller('SearchCtrl', function($scope, $http) {
    $scope.url = 'search.php'; //ur of search

    $scope.search = function() {
        //http request response
        $http.post($scope.url, {"data": $scope.keywords})
            .success(function(data, status) {
                $scope.status = status;
                $scope.data = data;
                $scope.result = data;
            })
            .error(function(data, status) {
                $scope.data = data || "Request failed";
                $scope.result = status;
            });
    };
});