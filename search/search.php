<?php
//request is JSON and has to be read as such

$data = file_get_contents("php://input");

$objData = json_decode($data);

//static array for now
$values = array('php', 'angular', 'js');

if(in_array($objData->data, $values)) {
    echo 'I have found what you are looking for';
} else {
    echo 'Sorry no match';
}