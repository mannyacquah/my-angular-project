var app = angular.module('myApp', []);
app.run(function($rootScope) {
    $rootScope.name = "Ari Lerner";
});
app.controller('MyController', function($scope) {
    $scope.person = {
        name: "Manny"
    };
    $scope.roommates = [
        {name: 'Ar'},
        {name: 'Kwasi'},
        {name: 'Test'}
    ];
    $scope.people = {
        'Ari' : 'Blue',
        'Kwasi' : 'black'
    };
    var updateClock = function() {
        $scope.clock = new Date();
    };
    var timer = setInterval(function() {
        $scope.$apply(updateClock());
    }, 1000);
    updateClock();
});
app.controller('ParentController', function($scope) {
    $scope.person = {
        greeted: false
    };
});
app.controller('ChildController', function($scope) {
    $scope.sayHello = function() {
        $scope.person.greeted = true;
        $scope.person.name = 'test';
    }
    $scope.reset = function () {
        $scope.person.name = 'Manny';
        $scope.person.greeted = false;
    }
});

var apiKey = 'YOUR_KEY',
    nprUrl = 'http://api.npr.org/query?id=61&fields=relatedLink,title,byline,text,audio,image,pullQuote,all&output=JSON';

app.controller('PlayerController',  function($scope, $http) {
    $scope.playing = false;
    $scope.audio = document.createElement('audio');
    $scope.audio.src = '/media/npr.mp4';
    $scope.play = function(program) {
        if ($scope.playing) {
            $scope.audio.pause();
        }
        var url = program.audio[0].format.mp4.$text;
        audio.src = url;
        audio.play();
        $scope.playing = true;
    };
    $scope.stop = function () {
        $scope.audio.play();
        $scope.playing = false;
    };
    $scope.audio.addEventListener('ended', function() {
        $scope.play(function() {
            $scope.stop();
        });
    });
    $http({
        method: 'JSONP',
        url: nprUrl + '&apiKey=' + apiKey + '&callback=JSON_CALLBACK'
    }).success(function(data, status) {
        // Now we have a list of the stories (data.list.story)
        // in the data object that the NPR API
        // returns in JSON that looks like:
        // data: { "list": {
        //   "title": ...
        //   "story": [
        //     { "id": ...
        //       "title": ...
        $scope.programs = data.list.title;
    }).error(function(data, status) {
        // Some error occurred
    });
});
app.controller('RelatedController', ['$scope', function($scope) {

}]);
app.controller('DemoController', ['$scope', function($scope) {
    $scope.counter = 0;
    $scope.add = function($amount) {
        $scope.counter += $amount;
    };
    $scope.subtract = function($amount) {
        $scope.counter -= $amount;
    };
}]);
app.directive('ngSparkline', function() {
   return {
       restrict: 'A',
       template: '<div class="sparkline"></div>'
   }
});
app.directive('nprLink', function() {
    return {
        restrict: 'EA',
        require: ['^ngModel'],
        replace: true,
        scope: {
            ngModel: '=',
            play: '&'
        },
        templateUrl: '/views/nprListItem.html',
        link: function (scope, ele, attr) {
            scope.duration = scope.ngModel.audio[0].duration.$text;
        }
    }
});

app.controller('SearchController', function($scope, $http) {
    $scope.url = 'search.php'; //ur of search

    $scope.search = function() {
        //http request response
        $http.post($scope.url, {"data": $scope.keywords})
            .success(function(data, status) {
                $scope.status = status;
                $scope.data = data;
                $scope.result = data;
            })
            .error(function(data, status) {
                $scope.data = data || "Request failed";
                $scope.result = status;
            });
    };
});